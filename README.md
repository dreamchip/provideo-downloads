# Introduction
This repository is used to host firmware updates and compiled software for ProVideo products by Dream Chip Technologies.

This readme is used as an overview of the supported devices and software downloads. By clicking one of the following links you will be redirected to a Wiki entry for that specific product, with further instructions.

# Supported Devices
The following devices are currently supported and will receive firmware updates in the future:
* **2K (Full HD) cameras:**
  * [ATOM one (xbow)](https://gitlab.com/dreamchip/provideo-downloads/wikis/ATOM-one)
  * [ATOM one mini (cooper)](https://gitlab.com/dreamchip/provideo-downloads/wikis/ATOM-one-mini)
  * [ATOM one SSM500 (caterham)](https://gitlab.com/dreamchip/provideo-downloads/wikis/ATOM-one-SSM500)
  * [ATOM one mini Zoom (blackline)](https://gitlab.com/dreamchip/provideo-downloads/wikis/ATOM-one-mini-Zoom)
* **4K (Ultra HD) cameras:**
  * [ATOM one 4K (condor4k)](https://gitlab.com/dreamchip/provideo-downloads/wikis/ATOM-one-4k) **Discontinued!**
  * [ATOM one 4K mini (condor4k_mini)](https://gitlab.com/dreamchip/provideo-downloads/wikis/ATOM-one-4k-mini)
* **Accessories**
  * [ATOM one Motor Drive (crossfire)](https://gitlab.com/dreamchip/provideo-downloads/wikis/ATOM-one-Motor-Drive)

# ProVideo Control Software
For easy device setup we supply an intuitive control software which is based on the QT framework. The GUI is generally compatible with all of the above listed devices, but support may vary between different versions. A compiled Windows binary of the GUI is supplied with each of the afore mentioned firmware releases. Alternatively you can always download the latest Linux and Windows releases here:

[ProVideo GUI Download Page](https://gitlab.com/dreamchip/provideo-downloads/wikis/provideo-gui)

The GUI is released open source, the source code can be downloaded here:

[ProVideo GUI Repository](https://gitlab.com/dreamchip/provideo-gui)

# Application Notes
Application notes provide further information beyond the content of the user manuals which are provided with the firmware updates. The following application notes are available:
* [Multi Camera Setup](https://gitlab.com/dreamchip/provideo-downloads/wikis/Multi-Camera-Setup): App Note on how to setup the RS485 bus and genlock network when using multiple ATOM one cameras
