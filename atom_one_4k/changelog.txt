----------------------------------------------------------------------------------
##################################################################################
#                              Atom One 4k Changelog                             #
##################################################################################

Newest Release:

Version ATOM_one_4k_V1_1_1
Date	25.09.2017

Firmware Revision   1849
Bitstream Revision  1844
GUI Version         1.0.6

Changes:
- Documentation:
	- Added description of "cam_info" command
- Firmware:
	- The "cam_info" command now also outputs the minimum ISO value of the camera
	  (at gain 1)
	- Fix for timecode which could cause undefined behavoir due to out of range
	  indexing
	- Fix for DPCC dumping duplicate pixels
	- Fix for spi-flash driver not using the correct erase command which was a
	  potential error cause
- Bootloader:
	- Turn on red LED to indicate bootloader is running
- FPGA:
	- Added tri-state buffer to SPI bus
	- Use new custom GTX
	- Fix for dpcc auto load feature: The number of pixels found register could
	  not be read
- GUI:
	- With the min ISO value which is now supplied by the "cam_info" command the
	  gain can now be correctly displayed in ISO
	
----------------------------------------------------------------------------------

Version ATOM_one_4k_V1_1_0
Date	08.09.2017

Firmware Revision   1807
Bitstream Revision  1765
GUI Version         1.0.5

Changes:
- Documentation:
	- Added desccription of the lens shading correction ("lsc") command
	- Added discription of "dpc_auto_load" command
	- Added description of "timecode_hold" command
- Firmware:
	- Added lens shading correction driver and commands ("lsc")
	- Added "dpc_test_mode" and "dpc_auto_load" commands
	- Updated build info script to generate a neutral version ID for the version
	  command
	- Fixed dpc save / load and add commands
	- Added "timecode_hold" command to pause the timecode (it will be incremented
	  in the background, but not output over SDI)
	- The de-gamma lookup table for the second SDI output now has its own set of
	  preset storages. This fixes a bug, where the GUI would not display the
	  correct lookup table when switchting bewteen the outputs.
	- Fixed WDR (knee function), values are now correctly applied and loading
	  on startup works as intended
	- The "temp" command will now show the CPU temperature
	- Hotfix for "startup with black screen"
- Bootloader:
	- Changed bootloader ID to make it brand neutral
	- Fix for overflow in the Watchdog timer which caused the camera to always
	  boot into the bootloader
- FPGA:
	- Added dpcc auto load feature
	- Fixed bugs in dpcc module that caused the y coordinate of given pixels
	  to be halved
	- Improved debayer for sharper images. Reduced aliasing effects
	- Reduced ramp effect on 1-2 pixel edges
- GUI:
	- Fixed an out of memory error by increasing communication buffer size
	- Increased dpcc table loading timeout
	- RAW 10 and 12 video modes were swapped
	- MCC Sliders in the MCC Eq Box can now be reset by double clicking them
	- Min and Max values of the MCC Sliders are now displayed in the MCC Eq Box
	- Added button to execute "dpc_auto_load" command from the DPCC Box
	- Fix for DPCC coordinates: Valid range is now 0 to resolution-1
	- Added "reset to defaults" button for master black and flare level settings
	- Changes to the DPC tab for easier usage and new test mode functionality
	- Minor changes to the info tab (now shows release / build date)
	- Added timecode "Hold" button to the in-out tab
	- GUI is now provided as a static executable (no more DLLs needed)
	- GUI includes new version of the flashloader tool

----------------------------------------------------------------------------------

Version ATOM_one_4k_V1_0_6
Date	16.06.2017

Firmware Revision   1609
Bitstream Revision  1602
GUI Version         1.0.1

Changes:
- Documentation:
	- Added name command description ("name")
- Firmware:
	- Added name command
	- Added driver for SDI cable driver and reclocker
- FPGA:
	- Use new optimized SDI transciever module
- GUI:
	- Support lens shading correction on the In/Out Tab (not supported by
	  ATOM one 4k yet)
	- Changes to support new device name feature

----------------------------------------------------------------------------------

Version ATOM_one_4k_V1_0_5
Date	10.04.2017

Changes:
- Documentation:
	- Updated mcc_blink command
	- Removed / updated rec709 commands
	- Added rs485_term command
- Firmware:
	- Added rs485_term command
	- Other small improvements concerning Gamma LUT setup
- FPGA: HW1324
- GUI:
	- New MCC control widget (MCC Equalizer)

----------------------------------------------------------------------------------

Version ATOM_one_4k_V1_0_4
Date	07.04.2017

Changes:
- Documentation: /
- Firmware:
	- Hide SD Card commands in help
	- Changed the default settings that are applied on camera reset
	- Fixed a bug, where the Gamma LUTs were not correctly reset in reset_settings
- FPGA: HW1324
- GUI: /

----------------------------------------------------------------------------------

Version ATOM_one_4k_V1_0_3
Date	05.04.2017

Changes:
- Documentation: /
- Firmware: /
- FPGA: HW1324
	- fixed flare compensation measurement module
- GUI:
	- re-enabled flare compensation

----------------------------------------------------------------------------------

Version ATOM_one_4k_V1_0_2
Date	04.04.2017

Changes:
- Documentation:
	- updated with new features
- Firmware:
	- added genlock feature
	- added driver for i2c led
	- disabled unused USB interface
	- improved interrupt handling
	- improved SPI read / write performance for big data blocks
	- fixes to the lut driver
	- added blink period to MCC command
	- fixed auto exposure
- FPGA: HW1294
	- fixes for genlock
	- updated sensor interface
	- improved reset behaviour
- GUI:
	- disabled flare compensation because it is not functional at the moment

----------------------------------------------------------------------------------

Version ATOM_one_4k_V1_0_1
Date	13.12.2016

Changes:
- Documentation:
	- initial version	
- Firmware:
	- initial version	
- FPGA: HW999
	- initial version	
- GUI:
	- initial version	

----------------------------------------------------------------------------------
